import pickle

# Read Train file
train_file = open('training_data.csv', 'r')
raw_train_data = train_file.readlines()
train_file.close()

train_data = []
correct_lines = 0
incorrect_lines = 0
for line in raw_train_data[1:]:
    if line.count(',') > 0:
        train_data.append([float(item) for item in line[:-1].split(',')])
        correct_lines += 1
    else:
        incorrect_lines += 1

print("train correct lines: ", correct_lines)
print("train incorrect lines: ", incorrect_lines)

with open("train_data", "wb") as fp:
    pickle.dump(train_data, fp)


# Read Test file
test_file = open('test_data.csv', 'r')
raw_test_data = test_file.readlines()
test_file.close()

test_data = []
for line in raw_test_data[1:]:
    test_data.append(float(line[:-1]))

print("test lines: ", len(test_data))

with open("test_data", "wb") as fp:
    pickle.dump(test_data, fp)
