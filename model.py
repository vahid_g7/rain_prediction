import pickle
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns

with open("train_data", "rb") as fp:
    train_data = pickle.load(fp)

with open("test_data", "rb") as fp:
    test_data = pickle.load(fp)

train_data.sort(key=lambda x: x[0])
X = [item[0] for item in train_data]
Y = [item[1] for item in train_data]
print(X)
print(Y)

dataset = pd.DataFrame(train_data, columns=['SNR', 'rain'])

# describe data
print("\ndataset head: \n", dataset.head())
print("\ndataset null: \n", dataset.isnull().any())
print("\ndataset data types: \n", dataset.dtypes)
print("\ndataset describe: \n", dataset.describe())


# plot histogram of data
sns.pairplot(dataset)
plt.show()
